const express = require("express");
const router = express.Router();
const todo = require("../Models/todo");

router.get("", async (req, res, next) => {
  try {
    const todos = await todo.find({}, { __v: 0 });
    return res.status(200).send({ status: "success", data: { todos } });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send({ status: "failed", message: error.message });
  }
});

router.post("", async (req, res, next) => {
  try {
    await todo.create(req.body);

    return res
      .status(201)
      .send({ status: "success", message: "Created successfully" });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send({ status: "failed", message: error.message });
  }
});

router.put("/:id", async (req, res, next) => {
  try {
    const { id } = req.params;

    const { modifiedCount } = await todo.updateOne(
      { _id: id },
      { $set: req.body }
    );

    if (!modifiedCount)
      return res
        .status(200)
        .send({ status: "failed", message: "Update failed" });

    return res
      .status(200)
      .send({ status: "success", message: "Updated successfully" });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send({ status: "failed", message: error.message });
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const { id } = req.params;

    const { deletedCount } = await todo.deleteOne({ _id: id });

    if (!deletedCount)
      return res.send({ status: "failed", message: "Delete failed" });

    return res.send({ status: "success", message: "Deleted successfully" });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send({ status: "failed", message: error.message });
  }
});

module.exports = router;
