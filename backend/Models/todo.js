const { Schema, model } = require("mongoose");

const todoSchema = new Schema(
  {
    name: String,
    completed: Boolean,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = model("todo", todoSchema);
