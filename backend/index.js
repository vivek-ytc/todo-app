require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const PORT = process.env.PORT || 3000;
const mongoURI = process.env.MONGO_URI;

const todoRoutes = require("./routes/todo");

mongoose
  .connect(mongoURI)
  .then(() => console.log("Connected to the MongoDB successfully."))
  .catch((error) => console.error(error.message));

app.use(cors());
app.use(express.json());
app.use("/todos", todoRoutes);

app.listen(PORT, () => console.log(`Server is listening on port ${PORT}`));
