import { useRef, useState } from "react";

const Modal = ({ addTodo }) => {
  const [todoName, setTodoName] = useState("");
  const modalRef = useRef();

  const onChange = ({ target }) => setTodoName(target.value);

  const onAdd = () => {
    addTodo(todoName);
    setTodoName("");
  };

  return (
    <div className="modal fade" ref={modalRef} id="new-todo-modal">
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="exampleModalLabel">
              Add Todo
            </h1>
            <button className="btn-close" data-bs-dismiss="modal" />
          </div>
          <div className="modal-body">
            <input
              autoFocus
              className="form-control"
              onChange={onChange}
              placeholder="Add new todo"
              type="email"
              value={todoName}
            />
          </div>
          <div className="modal-footer">
            <button
              className="btn btn-primary btn-sm"
              data-bs-dismiss="modal"
              disabled={!todoName}
              onClick={onAdd}
              type="button"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
