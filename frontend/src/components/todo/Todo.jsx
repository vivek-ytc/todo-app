import { useEffect, useRef } from "react";

import { FaEdit, FaTrash } from "react-icons/fa";
import { IoReload } from "react-icons/io5";
import { MdDoneOutline } from "react-icons/md";

import "./Todo.scss";

const Todo = ({ todo, onDelete, setTodoEditId, todoEditId, onEdit }) => {
  const inputRef = useRef();

  useEffect(() => {
    if (inputRef.current) inputRef.current.value = todo.name;
  }, [todoEditId]);

  const onKeyDown = ({ key }) =>
    key === "Enter" && onEdit(todoEditId, { name: inputRef.current.value });

  return (
    <main className="todo">
      <section className="name">
        {todoEditId === todo._id ? (
          <input
            className="form-control"
            onKeyDown={onKeyDown}
            placeholder="Edit todo name"
            ref={inputRef}
          />
        ) : (
          <p className={todo.completed ? "text-decoration-line-through" : ""}>
            {todo.name}
          </p>
        )}
      </section>

      <section>
        {todo.completed ? (
          <IoReload
            color="blue"
            className="icon"
            title="Mark as incomplete"
            onClick={() => onEdit(todo._id, { completed: false })}
          />
        ) : (
          <>
            <MdDoneOutline
              className="icon"
              color="green"
              title="Mark as complete"
              onClick={() => onEdit(todo._id, { completed: true })}
            />
            <FaEdit
              className="icon"
              title="Edit the todo"
              color="blue"
              onClick={() => setTodoEditId(todo._id)}
            />
          </>
        )}
        <FaTrash
          className="icon"
          color="red"
          title="Delete the todo"
          onClick={() => onDelete(todo._id)}
        />
      </section>
    </main>
  );
};

export default Todo;
