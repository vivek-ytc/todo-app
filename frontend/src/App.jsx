import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import "./App.scss";

import Modal from "./components/modal/Modal";
import Todo from "./components/todo/Todo";

import { baseURL } from "./config";

function App() {
  const [state, setState] = useState({
    copyTodos: [],
    todoEditId: "",
    todos: [],
  });

  useEffect(() => {
    getTodos();
  }, []);

  const getTodos = async () => {
    try {
      const response = await fetch(baseURL + "/todos");
      const { status, data, message } = await response.json();

      if (status !== "success") return alert(message);

      const todos = [];
      const completedTodos = [];

      data.todos.forEach((todo) => {
        if (todo.completed) completedTodos.push(todo);
        else todos.push(todo);
      });

      const finalTodos = todos.concat(completedTodos);

      handleStateUpdate({ todos: finalTodos, copyTodos: finalTodos });
    } catch (error) {
      console.error(error.message);
      alert(error.message);
    }
  };

  const addTodo = async (todo) => {
    try {
      const response = await fetch(baseURL + "/todos", {
        method: "post",
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify({ name: todo }),
      });

      const { status, message } = await response.json();

      if (status !== "success") {
        toast(message, { type: "error" });
        console.error(message);
        return;
      }

      getTodos();
    } catch (error) {
      toast(error.message, { type: "error" });
      console.error(error);
    }
  };

  const onEdit = async (id, payload) => {
    try {
      const response = await fetch(baseURL + "/todos/" + id, {
        method: "put",
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      const { status, message } = await response.json();

      if (status !== "success") {
        toast(message, { type: "error" });
        console.error(message);
        return;
      }

      handleStateUpdate({ todoEditId: null });
      getTodos();
    } catch (error) {
      toast(error.message, { type: "error" });
      console.error(error);
    }
  };

  const onDelete = async (id) => {
    try {
      const url = baseURL + "/todos/" + id;

      const response = await fetch(url, {
        method: "delete",
      });

      const { status, message } = await response.json();

      if (status !== "success") {
        toast(message, { type: "error" });
        console.error(message);
        return;
      }

      toast(message, { type: "success" });
      getTodos();
    } catch (error) {
      toast(error.message, { type: "error" });
      console.error(error.message);
    }
  };

  const onSearch = ({ value }) => {
    const regex = new RegExp(value, "i");

    const foundTodos = state.todos.filter(
      (todo) => !todo.completed && todo.name.match(regex)
    );

    handleStateUpdate({ copyTodos: foundTodos });
  };

  const setTodoEditId = (id) => handleStateUpdate({ todoEditId: id });

  const debounce = (func, delay) => {
    let timeoutId;

    return (...args) => {
      clearTimeout(timeoutId);

      timeoutId = setTimeout(() => func.apply(null, args), delay);
    };
  };

  const onSearchChange = debounce(({ target }) => onSearch(target), 1000);

  const handleStateUpdate = (newState) =>
    setState((prevState) => ({ ...prevState, ...newState }));

  return (
    <main className="container h-100 pt-5 overflow-auto">
      <div className="row align-items-center mb-3">
        <section className="col">
          <p className="title">Todo App</p>
        </section>
        <section className="col-auto">
          <button
            className="btn btn-primary btn-sm"
            data-bs-target="#new-todo-modal"
            data-bs-toggle="modal"
          >
            Add new todo
          </button>
        </section>
      </div>

      <input
        className="form-control"
        onChange={onSearchChange}
        placeholder="Search todo..."
        type="search"
      />

      <div className="todos">
        {state.copyTodos.map((todo, index) => {
          return (
            <Todo
              key={index}
              onDelete={onDelete}
              onEdit={onEdit}
              setTodoEditId={setTodoEditId}
              todo={todo}
              todoEditId={state.todoEditId}
            />
          );
        })}
      </div>

      <Modal addTodo={addTodo} />
    </main>
  );
}

export default App;
